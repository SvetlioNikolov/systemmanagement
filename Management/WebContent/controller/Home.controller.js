sap.ui.define([
	"sap/ui/demo/nav/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("sap.ui.demo.nav.controller.Home", {

		onDisplayNotFound : function () {
			// display the "notFound" target without changing the hash
			this.getRouter().getTargets().display("notFound", {
				fromTarget : "home"
			});
		},

		onNavToProducts : function () {
			this.getRouter().navTo("productList");
		},

		onNavToProductOverview : function (oEvent) {
			this.getRouter().navTo("productOverview");
		},
	
		onNavToAddMachine : function (oEvent) {
			this.getRouter().navTo("addMachine");
		}
	});

});
